// This is a basic Flutter widget test.
// To perform an interaction with a widget in your test, use the WidgetTester utility that Flutter
// provides. For example, you can send tap and scroll gestures. You can also use WidgetTester to
// find child widgets in the widget tree, read text, and verify that the values of widget properties
// are correct.

import 'package:flutter_test/flutter_test.dart';

void main() {
  test("Test regexp", () {
    var source = "background-image: url('https://pp.userapi.com/c618131/v618131017/3adc4/QCEvWcRodXo.jpg');";
    new RegExp(r"^.*\(\')").allMatches(source);
    source = source.replaceAllMapped(new RegExp(r"^.*\(\'"), (Match m) {
        return "$m";
    });
    //expect(source, "https://pp.userapi.com/c618131/v618131017/3adc4/QCEvWcRodXo.jpg");
  });
}
