import 'package:meta/meta.dart';

import 'models.dart';

@immutable
class AppState {
  final List<TrainingDay> trainingDays;

  AppState({
    this.trainingDays
  });

}