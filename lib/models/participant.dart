import 'package:meta/meta.dart';

@immutable
class Participant {
  final String name;
  final String avatar;

  Participant({
    this.name,
    this.avatar
  });

  @override
  String toString() {
    return 'Participant{name: $name, avatar: $avatar}';
  }

}