import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:html/parser.dart' show parse;

import 'models/models.dart';


void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Flutter Demo',
      theme: new ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: new MyHomePage(title: 'Crossfit Rekord'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<TrainingDay> _tabs = <TrainingDay>[];

  @override
  void initState() {
    super.initState();
    _getPage();
  }

  _getPage() async {
    final client = new HttpClient();
    var uri = new Uri.http('crossfitrekord.webox.beget.tech', 'rec.php');
    var request = await client.getUrl(uri);
    var response = await request.close();
    var responseBody = await response.transform(UTF8.decoder).join();
    final document = parse(responseBody);

    final trainingsMap = document.getElementsByClassName("w-tab-pane")
        .asMap()
        .map((index, tabPane) {
      final trainings = tabPane
          .getElementsByClassName("time-tab-link")
          .map((trainingElement) {
        return trainingElement.children[0].text;
      })
          .toList();

      return new MapEntry(index, trainings);
    });

    final trainingsDays = new List();
    for (final trainingDay in trainingsMap.entries) {
      final trainings = new List();
      await for (final training in _loadTraining(
          trainingDay.key, trainingDay.value)) {
        trainings.add(training);
      }
      trainingsDays.add(new TrainingDay(dayNum: trainingDay.key, dayName: "", date: "", trainings: trainings));
    }

    setState(() => _tabs = trainingsDays);
  }

  Stream<Training> _loadTraining(int index, List<String> timeList) async* {
    final client = new HttpClient();
    for (final time in timeList) {
      var uri = new Uri.http(
          'crossfitrekord.webox.beget.tech',
          'rec.php',
          {
            "day": index.toString(),
            "time": time
          }
      );
      var request = await client.getUrl(uri);
      var response = await request.close();
      var responseBody = await response.transform(UTF8.decoder).join();
      final document = parse(responseBody);

      final participants = document
          .getElementsByClassName("w-tab-pane")
          .where((element) => element.attributes["data-w-tab"] == "Tab $index")
          .first
          .getElementsByClassName("table-row")
          .map((row) {
        return new Participant(
            name: row.getElementsByClassName("username")[0].text.trim(),
            avatar: row.getElementsByClassName("userpic")[0].attributes["style"]
        );
      }).toList();

      yield new Training(time: time, participants: participants);
    }
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> children = _tabs.map((trainingDay) {
      return new Text('${trainingDay.dayName} : ${trainingDay.date}');
    }).toList() ?? <Widget>[];
    return new Scaffold(
        appBar: new AppBar(
          title: new Text(widget.title),
        ),
        body: new ListView.builder(
          itemBuilder: (BuildContext context, int index) {
            final trainings = _tabs[index].trainings.map((training) {
              final participants = training.participants.map((participant) {
                return new ListTile(title: new Text(participant.name));
              }).toList();
              return new ExpansionTile(
                title: new Text(training.time),
                children: participants,
              );
            }).toList();
            return new ExpansionTile(
              title: children[index],
              children: trainings,
            );
          },
          itemCount: children.length,
        )
    );
  }
}
